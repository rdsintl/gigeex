#include "utils.h"


void initialise_camera(GEV_CAMERA_HANDLE& handle)
{
	uint16_t status = 0;
	// initialise API
	printf("Initialising API...\n");
	status = GevApiInitialize();
	if (status != 0)
		throw std::runtime_error("Could not initialise API");

	// initialise camera
	// first determine number of camera's attached to system
	printf("Finding camera's on system\n");
	int camera_count = 0;
	camera_count = GevDeviceCount();
	if(camera_count<=0)
		throw std::runtime_error("Could not find any camera(s) on system\n");
	printf("Found %d camera(s)\n",camera_count);
	std::vector<GEV_CAMERA_INFO> camera_info(camera_count);
	int found_cameras = 0;
	// TODO: fix this for better pointer allocation
	status = GevGetCameraList(&camera_info[0],camera_count,&found_cameras);
	if(status != 0)
		throw std::runtime_error("Could not allocate camera info\n");
	printf("========== Camera ==============\n");
	printf("Manufacturer %s\n",camera_info[0].manufacturer);
	printf("Model %s\n",camera_info[0].model);
	// GEV_CAMERA_HANDLE handle = nullptr;
	// now using camera info open camera connection
	status = GevOpenCamera(&camera_info[0],GevExclusiveMode,&handle);
	if (status != 0)
		throw std::runtime_error("Could not open camera\n");
}

void clean_up(GEV_CAMERA_HANDLE* handle)
{
	//close camera connection
	GevCloseCamera(handle);
	GevApiUninitialize();
}

int get_camera_image_properties(Image_Properties& image_prop,
	GEV_CAMERA_HANDLE& camera_handle)
{
	//using camera handle first initialise genCamXmlfeature to
	// enable access to camera xml object
	uint16_t status = 0;
	status = GevInitGenICamXMLFeatures(camera_handle, TRUE);
	if (status != 0)
	{
		printf("Could not allocate pointer to genCamXMl\n");
		return 1;
	}

	// get pointer to nodeMap file on camera
	GenApi::CNodeMapRef *camera_map = static_cast<GenApi::CNodeMapRef*>(
		GevGetFeatureNodeMap(camera_handle));

	if (!camera_map)
	{
		printf("Could not allocate camera_map node object\n");
		return 1;
	}

	GenApi::CIntegerPtr ptrIntNode = camera_map->_GetNode("Width");
	image_prop._width = static_cast<uint16_t>(ptrIntNode->GetValue());
	ptrIntNode = camera_map->_GetNode("Height");
	image_prop._height = static_cast<uint16_t>(ptrIntNode->GetValue());
	GenApi::CIntegerPtr ptrInt = camera_map->_GetNode("OffsetX");
	image_prop._x_offset = static_cast<uint16_t>(ptrIntNode->GetValue());
	ptrInt = camera_map->_GetNode("OffsetY");
	image_prop._y_offset = static_cast<uint16_t>(ptrIntNode->GetValue());
	GenApi::CEnumerationPtr ptrEnumNode = camera_map->_GetNode("PixelFormat");
	image_prop._format = static_cast<uint32_t>(ptrEnumNode->GetIntValue());
	GenApi::CFloatPtr ptrframerate = camera_map->_GetNode("AcquisitionFrameRate");
	image_prop._fps = static_cast<uint16_t>(ptrframerate->GetValue());

	return 0;
}

int set_camera_interface_options(const GEV_CAMERA_HANDLE& camera_handle,
	const int num_buffers)
{
	//streamPktSize
	GEV_CAMERA_OPTIONS camOptions = {0};
	UINT16 status = GevGetCameraInterfaceOptions(camera_handle, &camOptions);
	if (status != 0)
		throw std::runtime_error("Could not object interface options\n");

	// set stream packet delay to 100 micro secs
	camOptions.streamPktDelay = 10;
	camOptions.streamNumFramesBuffered = static_cast<UINT32>(num_buffers);
	status = GevSetCameraInterfaceOptions(camera_handle,&camOptions);
	if (status!= 0)
		throw std::runtime_error("could not set interface options\n");


	GEV_CAMERA_OPTIONS cam2 = {0};
	status = GevGetCameraInterfaceOptions(camera_handle, &cam2);
	// if all goes well
	// print packetsize data
	printf("Stream Packetsize %d\n",cam2.streamPktSize);
	printf("Buffered frames %d\n",cam2.streamNumFramesBuffered);
	printf("stream memory limit %d\n",cam2.streamMemoryLimitMax);
	printf("Stream packet delay %d\n",cam2.streamPktDelay);

}

int set_image_dims_offset(const Image_Properties& image_prop, 
			const GEV_CAMERA_HANDLE& camera_handle)
{
	GenApi::CNodeMapRef *camera_map = static_cast<GenApi::CNodeMapRef*>(
		GevGetFeatureNodeMap(camera_handle));

	if (camera_map)
	{
		GenApi::CIntegerPtr ptrIntNode = camera_map->_GetNode("Width");
		ptrIntNode->SetValue(image_prop._width,true);	
		ptrIntNode = camera_map->_GetNode("Height");
		ptrIntNode->SetValue(image_prop._height,true);
		GenApi::CIntegerPtr ptrInt = camera_map->_GetNode("OffsetX");
		ptrInt->SetValue(image_prop._x_offset,true);
		ptrInt = camera_map->_GetNode("OffsetY");
		ptrInt->SetValue(image_prop._y_offset,true);
		return 0;
	}
	
	return 1;
}

// FIXME: this functionality must be implemented using c++ api, presently C api
// is showing some issue
void toggle_turbo_mode_on(const GEV_CAMERA_HANDLE& camera_handle,
const uint32_t value)
{
	int type;UINT32 val = 0;uint32_t status = 0;
	//first check if the transferturbo feature is available
	status = GevGetFeatureValue(
		camera_handle, 
		"transferTurboCurrentlyAbailable",
		&type, sizeof(UINT32), &val);
	// no this isn't a typo this is correct for some strange reason the word
	// is misspelled in camera settings
	printf("turbo status: %d val %d\n",status,val);
	val = 2;
	GevGetFeatureValue(camera_handle, "transferTurboMode", &type, sizeof(UINT32), &val);
	if (val == 1)
	{
		printf("Turbo transfer mode enabled.\n");
	}
	else
	{
		printf("Turbo transfer mode disabled, status %d enabling...\n",val);
		val = 1;
		GevSetFeatureValue(camera_handle, "transferTurboMode",
			sizeof(UINT32),&val);
		// val = 0;
		GevGetFeatureValue(camera_handle, "transferTurboMode", &type, sizeof(UINT32), &val);
		printf("Turbo Transfer mode. status: %d\n",val);
	}
}

int set_camera_frame_rate(const int frame_rate,GEV_CAMERA_HANDLE& camera_handle)
{
	GenApi::CNodeMapRef *camera_map = static_cast<GenApi::CNodeMapRef*>(
		GevGetFeatureNodeMap(camera_handle));

	if (camera_map)
	{
		GenApi::CFloatPtr ptrframerate = camera_map->_GetNode("AcquisitionFrameRate");
		uint16_t temp_fps = static_cast<uint16_t>(ptrframerate->GetValue());
		// if framerates are different then set value
		if (frame_rate!=temp_fps)
			ptrframerate->SetValue(static_cast<float>(frame_rate),true);
		return 0;
	}
	else{
		return 1;
	}
}
