#include "utils.h"

using namespace std;

int main(int argc, char* argv[])
{
	int buffer_size = 1;
	int user_width = 1920;
	int user_height = 1080;
	int required_fps = 53;
	int n_secs_video = 5;
	std::string video_file_name("extracted");
	// image pixel format
	const enumGevPixelFormat user_format = enumGevPixelFormat::fmtMono8;
	UINT8** frame_buffer = nullptr;
	GEV_BUFFER_OBJECT* buffer_object = nullptr;
	void** ptr_buffer_object = nullptr;
	av_register_all();//always set this up otherwise ffmpeg wont' work
	// duration of video in secs
	if (argc >= 2)
	{
		n_secs_video = std::atoi(argv[1]);
	}
	// setup width and height if provide in CLI
	if (argc >= 4)
	{
		user_width = std::atoi(argv[2]);
		user_height = std::atoi(argv[3]);
	}
	// set fps if supplied
	if (argc >= 5){
		//TODO: currently set to max default but change to 53 in turbo mode
		required_fps = std::atoi(argv[4]);
		if (required_fps > 53)
			required_fps = 53;
	}
	// setup buffersize if provided in CLI
	if (argc >= 6)
	{
		buffer_size = std::atoi(argv[5]);
	}

	// setup total number of buffers being used
	const int k_buffer_size = buffer_size;
	
	printf("Using %d image buffers.\n",k_buffer_size);
	printf("Image dims %d x %d.\n",user_width,user_height);
	printf("fps %d\n",required_fps);
	printf("video duration %d sec(s).\n",n_secs_video);

	uint16_t status = 0;
	GEV_CAMERA_HANDLE handle = nullptr;
	// invoke all camera initialisation properties and obtain a handle
	initialise_camera(handle);
	// set camera turbo mode on
	// toggle_turbo_mode_on(handle);//TODO: FIXME: see the issue in implementation @critical 	
	
	//all good then set required camera interface options
	//TODO: make this function argument based customisable
	set_camera_interface_options(handle);
	Image_Properties default_camera_image_prop;
	if (get_camera_image_properties(default_camera_image_prop,handle)!=0)
	{
		// if unsuccessfull clean up all resources and exit.
		printf("Could not obtain camera image properties, quiting...\n");
		clean_up(&handle);
	}

	printf("Default frame rate: %d\n",default_camera_image_prop._fps);
	// set new user defined framerate
	if (set_camera_frame_rate(required_fps,handle)==1)
	{
		printf("Could not set new framerate\n");
		clean_up(&handle);
	}
		
	Image_Properties new_properties;
	get_camera_image_properties(new_properties,handle);

	// determine image and offset dimension using prescan techniques to set
	// new image dimensions on camera
	uint32_t new_image_offset_x = default_camera_image_prop._width - user_width;
	uint32_t new_image_offset_y = default_camera_image_prop._height - user_height;
	// only set new image/frame dims if values of user and default differ
	if (user_width!=default_camera_image_prop._width && 
		user_width!=default_camera_image_prop._width)
	{
		status = GevSetImageParameters(
		handle,user_width,user_height,new_image_offset_x,new_image_offset_y,
		user_format);
		if (status!=0)
		{
			printf("could not set user defined image properties.\n");
			clean_up(&handle);
		}
	}


	// get image properties
	status = GevGetImageParameters(
			handle,&new_properties._width,&new_properties._height,
			&new_properties._x_offset,&new_properties._y_offset,
			&new_properties._format);

	std::cout << "User image properties \n" << 
		new_properties._width << " x " << new_properties._height <<
		"\noffsets " << new_properties._x_offset << " " << new_properties._y_offset <<
		"\nformat " << std::hex << new_properties._format << std::endl;

	// allocate frame buffer based on image properties
	// get image format in bytes using "API function"
	uint32_t depth = GetPixelSizeInBytes(new_properties._format);
	const int frame_size = new_properties._width * new_properties._height *
		depth;
	frame_buffer = new UINT8*[buffer_size];
	ptr_buffer_object = new void*[buffer_size];
	for (auto b=0;b<buffer_size;++b){
		frame_buffer[b] = new UINT8[frame_size];
		std::memset(frame_buffer[b],0,frame_size);
	}

	status = GevInitImageTransfer(handle,Asynchronous,
		static_cast<UINT32>(buffer_size),frame_buffer);
	if (status!=0)
	{
		std::cout << "could not initialise image transfer\n";
		clean_up(&handle);
		exit(-1);
	}

	status = GevStartImageTransfer(handle,-1);
	if (status!=0)
	{
		std::cout << "could not start image transfer\n";
		clean_up(&handle);
		exit(-1);		
	}

	// FFMPEG based video initialisation
	
	const AVRational r_fps = {static_cast<int>(new_properties._fps), 1};
	AVFormatContext* context = nullptr;
	std::string ffmpeg_video_file_name = video_file_name;
	ffmpeg_video_file_name += ".mp4";
	
	if (avformat_alloc_output_context2(
		&context,nullptr,nullptr,ffmpeg_video_file_name.c_str())<0)
    {
        std::cerr << "failed to allocate context\n";
        return 0;
    }
	printf("Format name %s\n",context->oformat->name);
	printf("Long name %s\n",context->oformat->long_name);
	printf("Mime %s\n",context->oformat->mime_type);
	printf("extensions %s\n",context->oformat->extensions);

	if (avio_open2(
		&context->pb,ffmpeg_video_file_name.c_str(), AVIO_FLAG_WRITE,
		nullptr, nullptr)<0
		)
    {
        std::cerr << "fail to avio_open2\n";
        return 0;
    }

	AVOutputFormat* format = context->oformat;
    // video codec
    AVCodec* codec = avcodec_find_encoder(format->video_codec);
    AVStream* vstrm = avformat_new_stream(context, codec);
    avcodec_get_context_defaults3(vstrm->codec,codec);
    // set stream values for video stream
    vstrm->codec->width = new_properties._width;
    vstrm->codec->height = new_properties._height;
    vstrm->codec->pix_fmt = codec->pix_fmts[0];
    vstrm->codec->time_base = vstrm->time_base = av_inv_q(r_fps);
    // vstrm->codec->bit_rate = 400000;
    vstrm->codec->gop_size = 10;
    vstrm->codec->max_b_frames = 1;
    vstrm->r_frame_rate = vstrm->avg_frame_rate = r_fps;
    if (format->flags & AVFMT_GLOBALHEADER)
    vstrm->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (avcodec_open2(vstrm->codec,codec,nullptr)<0)
    {
        std::cerr << "Failed to allocate avcodec_open2\n";
        return 0;
    }

    SwsContext* swsctx = nullptr;
    swsctx = sws_getCachedContext(
        nullptr,new_properties._width,new_properties._height,AV_PIX_FMT_GRAY8,
		new_properties._width,new_properties._height,vstrm->codec->pix_fmt,
		SWS_BICUBIC,nullptr,nullptr,nullptr);
    
    if (swsctx==nullptr) {
        std::cerr << "fail to sws_getCachedContext\n";
        return 0;
    }

    // std::cout << "pix formats \n codec " << codec->pix_fmts[0] << 
    // " vstream codec " << vstrm->codec->pix_fmt << std::endl;
    AVFrame* frame = av_frame_alloc();
    std::vector<uint8_t> framebuf(
        avpicture_get_size(vstrm->codec->pix_fmt,new_properties._width,
		new_properties._height)
    );
    std::cout << "buffer size " << framebuf.size() << std::endl;
    std::cout << "frame line size " << frame->linesize[0] << 
    " 1=> " << frame->linesize[1] << " " << 
    "2=> " << frame->linesize[2] << " " << 
    "3=> " << frame->linesize[3] << " " << std::endl;
    avpicture_fill(
        reinterpret_cast<AVPicture*>(frame),framebuf.data(),
        vstrm->codec->pix_fmt,new_properties._width,new_properties._height
    );
    
    frame->width = new_properties._width;
    frame->height = new_properties._height;
    frame->format = static_cast<int>(vstrm->codec->pix_fmt);
	int64_t frame_pts = 0;
	avformat_write_header(context, nullptr);

	cv::Mat image;//(new_properties._height,new_properties._width,CV_8UC1;

	// std::this_thread::sleep_for(1s);
	auto image_counter = 1;
	auto nframes = new_properties._fps*n_secs_video;
	for(auto f=0;f<nframes;++f)
	{
		status = GevWaitForNextImage(handle,&buffer_object,1000);
		// GevWaitForNextImageBuffer(handle,ptr_buffer_object,1000);
		if (buffer_object!=nullptr)
		{
			cv::Mat localimage(
				new_properties._height,new_properties._width,CV_8UC1,
				reinterpret_cast<void*>(buffer_object->address));
			// localimage.copyTo(image);
			const int stride[] = { static_cast<int>(localimage.step[0]) };
			sws_scale(swsctx,&localimage.data,stride,0,localimage.rows,frame->data,frame->linesize);
			frame->pts = frame_pts++;
        
			AVPacket pkt;
			pkt.data = nullptr;
			pkt.size = 0;
			int got_pkt = 0;
			av_init_packet(&pkt);
			if (avcodec_encode_video2(vstrm->codec,&pkt,frame,&got_pkt)<0)
			{
				std::cerr << "fail to allocate avcoded_encode\n";
				// exit;
				break;
			}//avcodec

			if (got_pkt)
			{
				// rescale packet timestamp
				pkt.duration = 1;
				av_packet_rescale_ts(&pkt,vstrm->codec->time_base,vstrm->time_base);
				// write packet
				av_write_frame(context, &pkt);
			}//got pkt

		}//buffer check ends here

	}//end loop
	
	av_write_trailer(context);
	av_frame_free(&frame);
    // free avcodec
    avcodec_close(vstrm->codec);
    // free context
    avio_close(context->pb);
	avformat_free_context(context);
	// stop image trainsfer
	GevStopImageTransfer(handle);

	// tidyup all Gev related resouces
	clean_up(&handle);

	for (auto b=0;b<buffer_size;++b)
	{
		delete[] frame_buffer[b];
	}
	
	return 0;
}

