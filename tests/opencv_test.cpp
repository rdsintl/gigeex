#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <iostream>

int main(int argc, char* argv[]) {
//    cv::VideoCapture vcap;
    cv::Mat image;

    // This works on a D-Link CDS-932L
//    const std::string rtsp = 
		//"rtsp://AxisUser:@xis12!@chs.dyndns-remote.com:554/axis-media/media.amp";
    const std::string rtsp = "http://169.254.192.225/?action=stream";
	const std::string sample_video("sample_video.mp4");

	int option = 20;

	if (argc>1 && argv[1])
	{
		option = std::atoi(argv[1]);
	}


	std::string source;
	if(option == 1)
		source = sample_video;
	else
		source = rtsp;

    //open the video stream and make sure it's opened
//    if(!vcap.open(source)) {
//        std::cout << "Error opening video stream or file" << std::endl;
//        return -1;
//    }

	double fps = 30;//vcap.get(CV_CAP_PROP_FPS);
	int width = 640;//vcap.get(CV_CAP_PROP_FRAME_WIDTH);
	int height = 480;//vcap.get(CV_CAP_PROP_FRAME_HEIGHT);

	cv::VideoWriter writer(
		"frames_sample.avi",CV_FOURCC('M','J','P','G'),fps,
		cv::Size(width,height),true);

	if(!writer.isOpened())
		throw std::runtime_error("Could not open video writer\n");


	auto n_frames = fps * option;
    for(auto f=1;f<=n_frames;++f)
	{
		std::string file_name("images/Frame_");
		file_name += std::to_string(f);
		file_name += ".jpg";
		image = cv::imread(file_name);
		writer.write(image);
    }   

	writer.release();
//	vcap.release();

	return 0;
}
