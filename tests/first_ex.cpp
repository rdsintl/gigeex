#include <iostream>
//#include "cordef.h"
#include "GenApi/GenApi.h"
#include "gevapi.h"
#include "GenICamVersion.h"
#include "GenICam.h"
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <memory>
#include <string>
#include <array>
#include <stdio.h>
#include <sstream>

#define BUFFER_SIZE 1


// simply check if the status returned is GEVLIB_OK
// otherwise return 0
int status_ok(GEV_STATUS status)
{
	if (status == GEVLIB_OK)
		return 0;
	else
		return 1;
}

int image_further_check(GEV_STATUS image_status)
{
	if (image_status == GEVLIB_ERROR_TIME_OUT)
	{
		std::cout << "Buffer timed out\n";
		return 1;
	}
	else if(image_status == GEVLIB_ERROR_NULL_PTR)
	{
		std::cout << "null pointer grabbed no data\n";
		return 1;
	}

	return 0;
}

int image_status_check(GEV_STATUS status)
{
	std::cout << "image_status_check: ";
	if (status_ok(status)==0)
	{
		std::cout << "image status okay\n";
		return 0;
	}
	else if(status == GEVLIB_ERROR_INVALID_HANDLE)
	{
		std::cout << "invalid handle\n";
		return 1;
	}
	else if(status == GEVLIB_ERROR_PARAMETER_INVALID)
	{
		std::cout << "parameter invalid\n";
		return 1;
	}
	else if(status == GEVLIB_ERROR_ARG_INVALID)
	{
		std::cout << "error argument invalid\n";
		return 1;
	}
	else if(status == GEVLIB_ERROR_SOFTWARE)
	{
		std::cout << "Some invalid software error occured\n";
		return 1;
	}
	else if(status == GEV_STATUS_BUSY)
	{
		std::cout << "Camera is busy, try again later\n";
		return 1;
	}

	return 0;

}

void clean_up(GEV_CAMERA_HANDLE* camera_handle)
{
	GevFreeImageTransfer(camera_handle);
	GevCloseCamera(camera_handle);
    GevApiUninitialize();
}

uint8_t print_image_properties(GEV_CAMERA_HANDLE* camera_handle)
{
	UINT32 img_width = 0;
	UINT32 img_height = 0;
	UINT32 img_x_offset = 0;
	UINT32 img_y_offset = 0;
	UINT32 img_format = 0;
	
	// obtain image properties
	GEV_STATUS camera_status = GevGetImageParameters(&camera_handle,&img_width,&img_height,&img_x_offset,&img_y_offset,&img_format);
	if(status_ok(camera_status)!=0)
	{
		std::cout << "Problems retrieving image properties\n";	
		return 1;
	}
	
	std::cout << "image properties " << 
		img_width << " x " << img_height <<
		"\noffsets " << img_x_offset << " " << img_y_offset <<
		"\nformat " << img_format << std::endl;
	return 0;
}


int main(int argc, char* argv[])
{
	std::cout << "Running genapi ...\n";		
    std::cout << "initialising API ...\n";
    GEV_STATUS status = -1;
    status = GevApiInitialize();
	if (status == GEVLIB_OK)
    	std::cout << "Successfully initialised API\n";

	status = -1;
	GEVLIB_CONFIG_OPTIONS* config_options = new GEVLIB_CONFIG_OPTIONS;
	status = GevGetLibraryConfigOptions(config_options);

	// if status ok then print some values
    if (status_ok(status) == 0)
    {
		std::cout << "============= Options ================\n";
		std::cout << "Version: " << config_options[0].version << "\n";
		std::cout << "Log Level: " << config_options[0].logLevel << "\n";
	}
	else {
		std::cerr << "some problem obtaining config options\n";
	}

	// get number of cameras attached to system
	int num_cameras = 0;
	num_cameras = GevDeviceCount();
	
	if (num_cameras > 0)
    {
		std::cout << "There are " << num_cameras << " camera(s) found\n";
	}
	else
	{
		std::cerr << "There are no camera(s) found\n";
	}

	// allocate n cameras array for gev_camera_info
	GEV_CAMERA_INFO* cameras = new GEV_CAMERA_INFO[num_cameras];
	int found_cameras = 0;

	// now create number of cameras
	GEV_STATUS camera_status = GevGetCameraList (cameras,num_cameras,&found_cameras);

	if (status_ok(camera_status)==1)
	{
		std::cerr << "COuld not allocate camera info\n";
		exit(-1);
	}
	

	// print some camera information
	std::cout << "========== Camera ==============\n";
	std::cout << "Manufacturer " << cameras[0].manufacturer << std::endl;
	std::cout << "Model " << cameras[0].model << std::endl;
	std::cout << "Serial " << cameras[0].serial << std::endl;
	std::cout << "Username " << cameras[0].username << std::endl;
	std::cout << "IPaddress " << cameras[0].ipAddr << std::endl;
	std::cout << "MacHigh " << cameras[0].macLow << std::endl;
	std::cout << "MacHigh " << cameras[0].macHigh << std::endl;
	PGEV_NETWORK_INTERFACE ptrnetworkinterface = &cameras[0].host;
	std::cout << "Ipaddress " << ptrnetworkinterface->ipAddr << std::endl;
	std::cout << "Ifindex " << ptrnetworkinterface->ifIndex << std::endl;

	// an instance of camera handle
	GEV_CAMERA_HANDLE camera_handle = nullptr;//new GEV_CAMERA_HANDLE;
	GevAccessMode access_mode = GevExclusiveMode;
	// open a connection directly with camera, one found above
	camera_status = GevOpenCamera(&cameras[0],access_mode,&camera_handle);

	status = GevInitGenICamXMLFeatures(camera_handle, TRUE);

	if (status_ok(status)==1)
	{
		std::cout << "issues with camxmlfeatures\n" << std::endl;
	}

	int32_t default_width = 0;
	int32_t default_height = 0;
	int32_t default_x_offset = 0;
	int32_t default_y_offset = 0;
	float default_frame_rate = 0;
	

	GenApi::CNodeMapRef *camera_map = static_cast<GenApi::CNodeMapRef*>(GevGetFeatureNodeMap(camera_handle));
	if (camera_map)
	{
		GenApi::CIntegerPtr ptrIntNode = camera_map->_GetNode("Width");
		default_width = (UINT32) ptrIntNode->GetValue();
		ptrIntNode = camera_map->_GetNode("Height");
		default_height = (UINT32) ptrIntNode->GetValue();
		GenApi::CIntegerPtr ptrInt = camera_map->_GetNode("OffsetX");
		default_x_offset = static_cast<UINT32>(ptrIntNode->GetValue());
		ptrInt = camera_map->_GetNode("OffsetY");
		default_y_offset = static_cast<UINT32>(ptrIntNode->GetValue());
		GenApi::CEnumerationPtr ptrEnumNode = camera_map->_GetNode("PixelFormat");
		int _format = (UINT32)ptrEnumNode->GetIntValue();
		GenApi::CFloatPtr ptrframerate = camera_map->_GetNode("AcquisitionFrameRate");
		default_frame_rate = ptrframerate->GetValue();
		std::cout << "Values from xml file\n";
		std::cout << "Width: " << default_width << std::endl;
		std::cout << "Height: " << default_height << std::endl;
		std::cout << "X-Offset: " << default_x_offset << std::endl;
		std::cout << "Y-Offset: " << default_y_offset << std::endl;
		std::cout << "Format: " << _format << std::endl;
		std::cout << "Framerate: " << default_frame_rate << std::endl;

	}
	else
	{
		std::cerr << "Could not allocate camera_map node object\n";
	}
	
	UINT32 img_width = 0;
	UINT32 img_height = 0;
	UINT32 img_x_offset = 0;
	UINT32 img_y_offset = 0;
	UINT32 img_format = 0;
	
	// obtain image properties
	camera_status = GevGetImageParameters(camera_handle,&img_width,&img_height,&img_x_offset,&img_y_offset,&img_format);

	if(status_ok(camera_status)!=0)
	{
		std::cout << "Problems retrieving image properties\n";	
		return 1;
	}
	
	std::string ans = (GevIsPixelTypeMono(img_format)? "Yes" : "No");
	std::cout << "Default image properties \n" << 
		img_width << " x " << img_height <<
		"\noffsets " << img_x_offset << " " << img_y_offset <<
		"\nformat " << std::hex << img_format <<
		"\nIsPixelMono " << ans << std::endl;

	int img_width_max = 1920;
	int img_height_max = 1080;
	int user_fps = 25;
	enumGevPixelFormat user_format = enumGevPixelFormat::fmtMono8;
	uint32_t pixelator = 0x01080008;
	// increase framerate and reduce memory footprint
	// set horizontal crop by setting x-offset and width values based on max width
	int img_set_x_offset = img_width - img_width_max;
	int img_set_y_offset = img_height - img_height_max;
	int img_set_width = img_width_max;
	int img_set_height = img_height_max;

	camera_status = GevSetImageParameters(
		camera_handle,img_set_width,img_set_height,img_set_x_offset,
		img_set_y_offset,user_format);

	if (status_ok(camera_status)!=0)
	{
		std::cout << "problems setting new image properties\n";
		exit(-1);
	}
	// obtain image properties
	camera_status = GevGetImageParameters(camera_handle,&img_width,&img_height,&img_x_offset,&img_y_offset,&img_format);

	if(status_ok(camera_status)!=0)
	{
		std::cout << "Problems retrieving image properties\n";	
		return 1;
	}

	UINT32 format_in_bytes = GetPixelSizeInBytes(img_format);
	
	std::cout << "User image properties \n" << 
		img_width << " x " << img_height <<
		"\noffsets " << img_x_offset << " " << img_y_offset <<
		"\nformat " << img_format << 
		"\n format (Bytes) " << format_in_bytes << std::endl;


	// allocate memory for 8 buffers simply a random values from example code
	const int image_size = img_width * img_height * format_in_bytes;
	UINT8** image_buffer = new UINT8*[BUFFER_SIZE];
//	UINT8** buffer_ptr= new UINT8*[BUFFER_SIZE];

	for (auto b=0;b<BUFFER_SIZE;++b){
		image_buffer[b] = new UINT8[image_size];
		std::memset(image_buffer[b],0,image_size);
//		buffer_ptr[b] = image_buffer[b];
	}
	
//	camera_status = GevInitializeImageTransfer(camera_handle,static_cast<UINT32>(BUFFER_SIZE),image_buffer);
	camera_status = GevInitImageTransfer(camera_handle,Asynchronous,static_cast<UINT32>(BUFFER_SIZE),image_buffer);
	if (image_status_check(camera_status)!=0)
	{
		std::cout << "could not initialise image transfer\n";
		exit(-1);
	}

	camera_status = GevStartImageTransfer(camera_handle,-1);
	if (image_status_check(camera_status)!=0)
	{
		std::cout << "could not start image transfer\n";
		exit(-1);		
	}

	if (image_further_check(camera_status)!=0)
	{
		std::cout << "Further check issues\n";
		exit(-1);
	}

	printf("Size of GEV_BUFFER_OBJECT %ld\n",sizeof(GEV_BUFFER_OBJECT));
	cv::waitKey(1000);
	cv::VideoWriter videowriter(
		"sample.avi",CV_FOURCC('M', 'J', 'P', 'G'),static_cast<double>(30),cv::Size(img_width,img_height),false
	);
	if (!videowriter.isOpened())
	{
		std::cerr << "could not open video writer for creating video...\n";
		clean_up(&camera_handle);
		exit(-1);
	}
	cv::Mat img;
	for(;;)
	{
		GEV_BUFFER_OBJECT* image_object = nullptr;
		GEV_STATUS image_status = GevWaitForNextImage(camera_handle,&image_object,1000);
		if (image_object!=nullptr)
		{
			std::string str("timestamp: ");					
			cv::Mat img1(img_height,img_width,CV_8UC1,image_object->address);
			img1.copyTo(img);
			cv::cvtColor(img1,img,CV_BayerRG2RGB);
			videowriter.write(img1);			
			cv::Mat img2(img_height,img_width,CV_8UC1,&image_object->address[1]);
			str += std::to_string(image_object->timestamp_lo);
			cv::putText(
					img,str,cv::Point2f(100,100),
					CV_FONT_HERSHEY_PLAIN, 1,  cv::Scalar(0,255,0,255),1.5);

		}
		if (img.empty()){
			cv::waitKey(10);
			printf("Continuing...");
			continue;
		}
		cv::imshow("image 1",img);
		if(cv::waitKey(1) == 'q') break;
	}

	videowriter.release();
/*	std::cout << "Resetting image properties...\n";
	camera_status = GevSetImageParameters(
		camera_handle,img_width,img_height,img_x_offset,
		img_y_offset,img_format);
	if(status_ok(camera_status)!=0)
	{
		std::cout << "Problems resetting image properties\n";	
		return 1;
	}*/
	GevAbortImageTransfer(camera_handle);
	GevStopImageTransfer(camera_handle);
	clean_up(&camera_handle);
	for (auto d=0;d<static_cast<int>(BUFFER_SIZE);++d)
		delete[] image_buffer[d];
	return 0;
}


























