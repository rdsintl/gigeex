
#include "cordef.h"
#include "gevapi.h"
//#include "GenApi/GenApi.h"
#include "opencv/cv.h"

#include <stdio.h>

// simply check if the status returned is GEVLIB_OK
// otherwise return 0

int status_check(GEV_STATUS status);
int status_check(GEV_STATUS status)
{
	if(status == GEVLIB_OK)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

int main(int argc, char* argv[])
{
    GEV_STATUS status = GevApiInitialize();
	if (status == GEVLIB_OK)
	{
		printf("Api initialised\n");
	}

	GEVLIB_CONFIG_OPTIONS config_options = {0};
	status = GevGetLibraryConfigOptions(&config_options);

	// if status ok then print some values
    if (status == GEVLIB_OK)
    {
		printf("Version: %d\n",config_options.version);
		printf("Log Level: %d\n",config_options.logLevel);
		// change log level format
		config_options.logLevel = GEV_LOG_LEVEL_ERRORS;
		config_options.logLevel = GEV_LOG_LEVEL_DEBUG;
		status = GevSetLibraryConfigOptions(&config_options);
		if (status == GEVLIB_OK)
			printf("Log levels changed\n");
	}
	else
	{
		printf("Some problem getting library config options\n");
	}


	int num_cameras = 0;
	num_cameras = GevDeviceCount();
	
	if (num_cameras > 0)
    {
		printf("There are %d camera(s) found\n",num_cameras);
	}
	else
	{
		printf("There are no camera(s) found\n");
		exit(-1);
	}

	// now look for camera information
	GEV_DEVICE_INTERFACE cameras[num_cameras];// = {0};
	int found_cameras = 0;

	// now create number of cameras
	GEV_STATUS camera_status = GevGetCameraList(cameras,num_cameras,&found_cameras);

	if (status_check(camera_status)==0)
	{
		printf("number of cameras found are %d\n",found_cameras);
	}
	else
	{
		printf("COuld not allocate camera info\n");
		exit(-1);
	}
	

	// print some camera information
	printf("========== Camera ==============\n");
	printf("Manufacturer: %s\n",cameras[0].manufacturer);
	printf("Model: %s\n",cameras[0].model);
	printf("Serial: %s\n",cameras[0].serial);


	// camera handle allocation
	// an instance of camera handle
	GEV_CAMERA_HANDLE camera_handle;//new GEV_CAMERA_HANDLE;
	// open a connection directly with camera, one found above
	camera_status = GevOpenCamera(&cameras[0],GevExclusiveMode,&camera_handle);
	if (status_check(camera_status)==1)
	{
		printf("Problem allocating camera handle\n");
		exit(-1);
	}

	// initialise xml data connection
	// Set up feature access using the XML file retrieved from the camera
	camera_status = GevInitGenICamXMLFeatures(camera_handle, TRUE);

	if (status_check(camera_status)==1)
	{
		printf("issues with camxmlfeatures\n");
	}


	// xmlfilename pointer
	char xml_file_name[255];
	status = GevGetGenICamXML_FileName(
		camera_handle,(int)sizeof(xml_file_name),xml_file_name
	);
	
	if (status_check(status) == 0)
	{
		printf("name of xml file found %s\n",xml_file_name);
	}
	else
	{
		printf("name of file not found\n");
	}

	// camera interface options
	GEV_CAMERA_OPTIONS camera_options = {0};
	camera_status = GevGetCameraInterfaceOptions (camera_handle,&camera_options);
	if (status_check(camera_status)==1)
	{
		printf("Could not obtain camera options\n");
	}

	camera_options.heartbeat_timeout_ms = 90000;
	camera_status = GevSetCameraInterfaceOptions(camera_handle,&camera_options);
	

	// dimensions of image camera image
	int width = 0;
	int height = 0;
	int format = 0;
	GevGetFeatureValue(camera_handle, "Width", &type, sizeof(width), &width);
	GevGetFeatureValue(camera_handle, "Height", &type, sizeof(height), &height);
	GevGetFeatureValue(camera_handle, "PixelFormat", &type, sizeof(format), &format);

//	CvMat* cvmat = cvCreateMat(640,480,CV_8UC1);

	GevCloseCamera(&camera_handle);
	GevApiUninitialize();
	return 0;
}


























