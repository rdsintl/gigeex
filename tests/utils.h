#include "GenApi/GenApi.h"
#include "gevapi.h"
#include <opencv2/opencv.hpp>
// #include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc/imgproc.hpp>

// FFmpeg
extern "C" {
    #include "libavformat/avformat.h"
    #include "libavcodec/avcodec.h"
    #include "libavutil/avutil.h"
    #include "libavutil/frame.h"
    #include "libswscale/swscale.h"
}

#include <memory>
#include <string>
#include <array>
#include <stdio.h>
#include <sstream>
#include <iostream>
#include <thread>

/* structure to store image properties */
typedef struct {
	uint32_t _format;
	uint32_t _width;
	uint32_t _height;
	uint32_t _x_offset;
	uint32_t _y_offset;
	uint32_t _fps;

} Image_Properties;


/* function to initialise all settings related with available camera properties
 * find camera
 * open camera
 * return camera handle
*/
void initialise_camera(GEV_CAMERA_HANDLE& handle);

/**
 * cleanup all the initialised camera properties to release all resources
 */
void clean_up(GEV_CAMERA_HANDLE* handle);


/**
 * function - get_camera_image_properties
 * get all image dimension defined in camera 
 * height
 * width
 * x_offset
 * y_offset
 * frame rate
 * format
 */

int get_camera_image_properties(Image_Properties& image_prop,
	GEV_CAMERA_HANDLE& camera_handle);

/**
 * set_image_dims_and_offset
 * this function set image dims and offset, offset is the lines where
 * the camera produces sensor to start image
 */
int set_image_dims_offset(const Image_Properties& image_prop, 
			const GEV_CAMERA_HANDLE& camera_handle);

/**
 * set_camera_frame_rate
 * use this function to set user defined frame rate for frame acquisition
 */
int set_camera_frame_rate(const int frame_rate,GEV_CAMERA_HANDLE& camera_handle);

/**
 * set_camera_interface_options
 * this function is helpful in determining and setting network interfacing 
 * between camera and ethernet adapter to match MTU limit on NIC and also delay
 * in delivering packets from camera to network interface moreover also setting
 * buffers etc.
 */
int set_camera_interface_options(
	const GEV_CAMERA_HANDLE& camera_handle,
	const int num_buffers = 1); 

/**
 * toggle_turbo_mode_on
 * this function will set camera turbo feature on, this feature enables 
 * fast and efficient data transfer from camera to ethernet network
 * using a compression technique which enables FPS speedup upto 53 fps
 * initially it checks for if the feature is available and then
 * obtains the default value to make it enable
 */
void toggle_turbo_mode_on(const GEV_CAMERA_HANDLE& camera_handle, 
const uint32_t value = 1);//default turns it on


